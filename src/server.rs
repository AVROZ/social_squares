use std::io::prelude::*;
use std::io::ErrorKind;
use std::io::Error;
use std::net::{TcpListener, TcpStream};
use std::thread;
use std::sync::mpsc::{channel,Sender,Receiver};
use std::collections::HashMap;

enum Msg {
    Up,
    Down,
    Left,
    Right,
    Connect(Sender<HashMap<u8, (i32, i32)>>),
    Disconnect,
}


fn write_squares( squares: HashMap<u8,(i32,i32)>, stream : &mut TcpStream) {
    let n = squares.len() as u8; // TODO: limit the number of connection to the maximal u8 value in the appropriate place
    stream.write_all(&[n]).unwrap(); // TODO: is this .unwrap() appropriate ?
    for (id,p) in squares.iter() {
        let mut b = [ 0u8; 9];
        b[0] = *id;
        b[1] = (((*p).0 >> 24) & 0xFF) as u8;
        b[2] = (((*p).0 >> 16) & 0xFF) as u8;
        b[3] = (((*p).0 >> 8) & 0xFF) as u8;
        b[4] = (((*p).0 >> 0) & 0xFF) as u8;
        b[5] = (((*p).1 >> 24) & 0xFF) as u8;
        b[6] = (((*p).1 >> 16) & 0xFF) as u8;
        b[7] = (((*p).1 >> 8) & 0xFF) as u8;
        b[8] = (((*p).1 >> 0) & 0xFF) as u8;
        stream.write_all( &b).unwrap(); // TODO: is this .unwrap() appropriate ?
    }
}

fn client_listener(mut stream: TcpStream, id : u8, sender: Sender<(u8,Msg)>) {
    let (response_sender, response_receiver) = channel();
    sender.send((id, Msg::Connect(response_sender))).unwrap(); // TODO: is this .unwrap() appropriate ?
    stream.write_all(&[id]).unwrap(); // TODO: is this .unwrap() appropriate ?

    let mut b = [0];
    loop {
        match stream.read_exact(&mut b) {
            Ok(()) => {
                let msg = match b[0] {
                    0 => Msg::Up,
                    1 => Msg::Down,
                    2 => Msg::Left,
                    3 => Msg::Right,
                    _ => continue,
                };
                sender.send((id, msg)).unwrap();

                if let Ok(squares) = response_receiver.try_recv() {
                    write_squares(squares, &mut stream);
                }
            },
            Err(e) => {
                if e.kind() == ErrorKind::UnexpectedEof {
                    sender.send((id, Msg::Disconnect)).unwrap();
                    break;
                }
                panic!("Client listener error: {:?}", e);
            }
        }
    }
}

fn model_holder(receiver: Receiver<(u8, Msg)>) {
    let mut squares = HashMap::<u8, (i32, i32)>::new();
    let mut client_senders = HashMap::new();
    
    for (id, msg) in receiver.iter() {
        match msg {
            Msg::Up => {
                squares.entry(id).and_modify(|p| if p.1 > 0 { p.1 -= 1 });
            },
            Msg::Down => {
                squares.entry(id).and_modify(|p| p.1 += 1);
            },
            Msg::Left => {
                squares.entry(id).and_modify(|p| if p.0 > 0 { p.0 -= 1 });
            },
            Msg::Right => {
                squares.entry(id).and_modify(|p| p.0 += 1);
            },
            Msg::Connect(sender) => {
                squares.insert(id, (0, 0));
                client_senders.insert(id, sender);
            },
            Msg::Disconnect => {
                squares.remove(&id);
                client_senders.remove(&id);
            },
        }

        for sender in client_senders.values() {
            sender.send(squares.clone()).unwrap();
        }
    }
}

fn main() {
    let listener = TcpListener::bind("localhost:1234").unwrap();  // TODO: is this .unwrap() appropriate ?
    let (sender, receiver) = channel();
    thread::spawn( move || model_holder( receiver));
    let mut next_id = 0;
    for stream in listener.incoming() {
        let stream = stream.unwrap(); // TODO: is this .unwrap() appropriate ?
        let id = next_id;
        let new_sender = sender.clone();
        thread::spawn( move || client_listener( stream, id, new_sender));
        next_id += 1;
    }
}
