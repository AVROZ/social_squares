use std::{io::prelude::*, collections::HashMap};
use piston_window::*;
use std::net::TcpStream;

fn read_squares( stream : &mut TcpStream) -> HashMap<u8,(i32,i32)>{
    let mut n : [u8; 1] = [0];
    stream.read_exact(&mut n).unwrap();  // TODO: is this .unwrap() appropriate ?
    let n = n[0];
    let mut res = HashMap::<u8,(i32,i32)>::with_capacity(n.into());
    for _ in 0 .. n {
        let mut b : [u8; 9] = [0; 9];
        stream.read_exact(&mut b).unwrap();  // TODO: is this .unwrap() appropriate ?
        res.insert(b[0], (
            ((b[1] as i32) << 24) + ((b[2] as i32) << 16) + ((b[3] as i32) << 8)  + (b[4] as i32),
            ((b[5] as i32) << 24) + ((b[6] as i32) << 16) + ((b[7] as i32) << 8)  + (b[8] as i32)));
    };
    res
}

fn main() {
    let mut stream = TcpStream::connect("localhost:1234").unwrap(); // TODO: is this .unwrap() appropriate ?
    println!("Connecté");
    let mut b = [0];
    stream.read_exact(&mut b).unwrap();  // TODO: is this .unwrap() appropriate ?
    println!("Connecté");
    let id = b[0];
    let title = format!("Social Squares client n°{id}");

    let mut window: PistonWindow =
        WindowSettings::new( title, [500, 500])
        .build().unwrap(); // TODO: is this .unwrap() appropriate ?
    window = window.exit_on_esc(true);
    println!("Window created");
    while let Some(e) = window.next() {
        if let Event::Input(Input::Button(b), _) = e {
            if b.state == ButtonState::Press {
                if let Button::Keyboard(k) = b.button {
                    match k {
                        Key::Up => {
                            stream.write_all(&[0]).unwrap();
                        },
                        Key::Down => {
                            stream.write_all(&[1]).unwrap();
                        },
                        Key::Left => {
                            stream.write_all(&[2]).unwrap();
                        },
                        Key::Right => {
                            stream.write_all(&[3]).unwrap();
                        },
                        _ => ()
                    }
                }
            }
        } else {
            stream.write_all(&[6]).unwrap();                            
            let squares = read_squares(&mut stream);
            let squares = &squares;
            window.draw_2d(&e, move |c, g, _| {
                clear([0.5, 0.5, 0.5, 1.0], g);
                const RED : [f32; 4] = [1.0, 0.0, 0.0, 1.0];
                const BLUE : [f32; 4] = [0.0, 0.0, 1.0, 1.0];
                for (i,(x,y)) in squares.iter() {
                    let color = if *i == id { RED } else { BLUE };
                    let rect = [*x as f64 * 20.0, *y as f64 * 20.0, 20.0, 20.0];
                    rectangle(color, rect, c.transform, g);
                }
            });
        }
    }
}
